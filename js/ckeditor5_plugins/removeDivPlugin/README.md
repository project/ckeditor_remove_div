# CKEditor 5 Removediv Plugin

## Overview
This repository contains the Removediv plugin for CKEditor 5, enabling the removal of `div` elements from the editor's content.

## Structure
The plugin is structured into several modules, each addressing specific functionalities:

- `index.js`: The main entry point for the plugin, importing and exporting the Removediv plugin.
- `removediv.js`: Main plugin class, integrating editing and UI components.
- `removedivcommand.js`: Command logic for removing `div` elements.
- `removedivediting.js`: Editing functionalities, including schema definitions.
- `removedivui.js`: User interface components, such as toolbar buttons.

## JavaScript Compiling Process
Source code in the `/src` directory is compiled into the CKEditor 5 plugin located in `/build`. While all code can be merged into `index.js`, a modular approach enhances maintainability.

## Installation and Usage
1. Place the plugin files in the `src` directory of your CKEditor 5 build.
2. Compile the plugin using CKEditor's build tools.
3. Include the plugin in your CKEditor 5 configuration for use.

## Building the Plugin
Execute `yarn build` in the root module directory to compile the plugin.

## Contributing
Contributors should follow established coding and documentation standards.

## Additional Information
Refer to the CKEditor 5 official documentation for guidance on using custom plugins.
