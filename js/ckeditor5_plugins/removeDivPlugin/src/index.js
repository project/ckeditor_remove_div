/**
 * @file
 * The main entry point for CKEditor 5 to recognize and load plugins.
 * This file is crucial in the build process, as CKEditor 5 identifies
 * available plugins through it. Multiple plugins can be consolidated and
 * exported from this single file.
 *
 * @description
 * Acts as a bridge between CKEditor 5 and custom plugins. It imports
 * and exports the necessary plugins, making them accessible to the editor.
 */

// Importing the RemoveDiv plugin from the RemoveDiv.js file.
import RemoveDiv from './removediv';

// Exporting the RemoveDiv plugin for CKEditor 5 to recognize and use.
export default {
  RemoveDiv: RemoveDiv,
};
