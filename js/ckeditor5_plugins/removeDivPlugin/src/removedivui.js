import {Plugin} from 'ckeditor5/src/core';
import {ButtonView} from 'ckeditor5/src/ui';
import icon from '../../../../icons/eraser.svg';

/**
 * @class RemoveDivUI
 * @extends Plugin
 * @description
 * This class is responsible for the user interface part of the RemoveDiv plugin in CKEditor 5.
 * It handles the creation and configuration of a toolbar button that allows users to execute
 * the `removeDiv` command. The button is equipped with an icon, label, and tooltip.
 */
export default class RemoveDivUI extends Plugin {
  /**
   * Initializes the UI component of the plugin. This method is responsible for
   * adding the `removeDiv` button to the editor's toolbar and setting up its properties
   * and behavior.
   */
  init() {
    const editor = this.editor;
    const t = editor.t;

    // Add the removeDiv button to the toolbar
    editor.ui.componentFactory.add('removeDiv', () => {
      const button = new ButtonView();

      button.set({
        label: t('Remove Div'),
        icon: icon, // Set the icon path
        tooltip: true
      });

      // Execute the command when the button is clicked
      button.on('execute', () => {
        editor.execute('removeDiv');
        editor.editing.view.focus();
      });

      return button;
    });
  }
}
