import {Command} from 'ckeditor5/src/core';

/**
 * @class RemoveDivCommand
 * @extends Command
 * @description
 * This class creates a command for removing `div` elements from the editor's content.
 * It extends CKEditor 5's built-in Command class.
 */
export default class RemoveDivCommand extends Command {
  /**
   * Refreshes the state of the command. It is called automatically by the editor
   * to decide whether the command should be enabled or disabled based on the current context.
   */
  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;

    // Assuming 'div' elements are represented by a specific model element (e.g., 'htmlDivParagraph')
    const position = selection.focus || selection.anchor;
    const divElement = position && (position.findAncestor('htmlDivParagraph') || position.findAncestor('paragraph'));
    const ancestors = position.getAncestors();

    // Enable the command only if the selection is within a 'div' element or its equivalent
    this.isEnabled = this._hasDivAncestor(ancestors);
  }

  /**
   * Removes a `div` element from the editor's content, handling it differently based on its content:
   * - If the `div` contains only text, it is replaced with a paragraph (`<p>`) containing the same text.
   * - If the `div` contains nested elements, those elements are moved outside the `div`, preserving their structure.
   * The function first identifies a `div` ancestor of the current selection. If found, it checks the `div`'s content and
   * applies the appropriate handling before removing the `div` from the document.
   */
  execute() {
    const model = this.editor.model;
    const selection = model.document.selection;

    model.change(writer => {
      const ancestors = selection.focus.getAncestors();
      const divElement = this._findDivAncestor(ancestors);

      if (divElement) {
        // Get the position before the div element to start reinserting its content
        const positionBeforeDiv = writer.createPositionBefore(divElement);

        const children = Array.from(divElement.getChildren());
        // Check if the div contains only text or is effectively standalone
        const isStandaloneText = children.every(child => child.is('text'));

        if (isStandaloneText) {
          // Convert the standalone div to a paragraph
          const paragraph = writer.createElement('paragraph');
          writer.insert(paragraph, positionBeforeDiv);
          // Move the text into the paragraph
          for (const child of children) {
            writer.append(child, paragraph);
          }
        } else {
          // For divs with nested elements, move each direct child to the position before the div
          for (const child of children) {
            writer.move(writer.createRangeOn(child), positionBeforeDiv);
          }
        }

        // After handling the children, remove the div element
        writer.remove(divElement);
      }
    });
  }

  /**
   * Checks if any of the ancestors in the given array is a div element.
   *
   * @param {Array<Object>} ancestors - An array of ancestor elements. Each element is an object representing a node in the CKEditor model tree.
   * @returns {boolean} Returns true if at least one ancestor is a div element (case-insensitive check on the element's name), false otherwise.
   */
  _hasDivAncestor(ancestors) {
    return ancestors.some(ancestor => {
      if (typeof ancestor === 'object' && 'name' in ancestor) {
        return ancestor.name.toLowerCase().includes('div');
      }
      return false;
    });
  }

  /**
   * Finds and returns the first ancestor from the given array that is a div element.
   *
   * This function iterates through the array of ancestors and returns the first element that is identified as a div (based on the 'name' property).
   * The check is case-insensitive, meaning it does not matter whether the div is represented as 'DIV', 'div', or any other case variation.
   * If no div ancestor is found, the function returns null.
   *
   * @param {Array<Object>} ancestors - An array of ancestor elements, where each element is an object with properties representing attributes of a node in the CKEditor model tree.
   * @returns {Object|null} The first ancestor object that represents a div element, or null if no such ancestor is found.
   */
  _findDivAncestor(ancestors) {
    for (const ancestor of ancestors) {
      if (typeof ancestor === 'object' && 'name' in ancestor && ancestor.name.toLowerCase().includes('div')) {
        return ancestor; // Return the first ancestor that satisfies the condition
      }
    }
    return null; // Return null if no such ancestor is found
  }
}
