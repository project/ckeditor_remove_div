import {Plugin} from 'ckeditor5/src/core';
import RemoveDivCommand from "./removedivcommand";

/**
 * @class RemoveDivEditing
 * @extends Plugin
 * @description
 * This class handles the editing aspect of the RemoveDiv plugin. It is responsible
 * for extending the CKEditor 5 model schema to include functionalities specific to
 * the handling of `div` elements. The class may include schema extensions or
 * converter definitions necessary for the proper functioning of the RemoveDiv command.
 */
export default class RemoveDivEditing extends Plugin {
  /**
   * Initializes the plugin's editing behavior. This method sets up the schema
   * extensions and converters for handling `div` elements within the editor's model.
   * It registers the 'removeDiv' command in the editor using the RemoveDivCommand class.
   */
  init() {
    const editor = this.editor;
    // const model = editor.model;
    this.editor.commands.add('removeDiv', new RemoveDivCommand(this.editor));
  }
}
