import {Plugin} from 'ckeditor5/src/core';
import RemoveDivEditing from './removedivediting';
import RemoveDivUI from './removedivui';

/**
 * @class RemoveDiv
 *
 * @description
 * This class represents the RemoveDiv plugin in CKEditor 5. It is responsible for
 * integrating the RemoveDiv functionality into the CKEditor 5 editor. The plugin
 * brings together editing functionalities and user interface components required
 * to enable the removal of 'div' elements from the editor content.
 */
export default class RemoveDiv extends Plugin {
  /**
   * Specifies the required dependencies for this plugin.
   * @returns {Array} An array of required classes.
   */
  static get requires() {
    return [RemoveDivEditing, RemoveDivUI];
  }

  /**
   * Provides the name of the plugin. This is used by CKEditor 5 for plugin identification.
   * @returns {string} The name of the plugin.
   */
  static get pluginName() {
    return 'RemoveDiv';
  }

}
